# Programación declarativa y funcional
```plantuml
@startmindmap
 *[#Yellow] Programacion declarativa y funcional
	**[#Gray] ¿Que es?
		***[#Pink] Es un estilo de programación o paradigma de programación
	**[#Gray] Su función:
		***[#Pink] Liberar asignaciones de memoria
	**[#Gray] Características
		***[#Pink] Las tareas rutinarias son del compilador
		***[#Pink] Hace énfasis en aspectos creativos
	**[#Gray] Ventajas
		***[#Pink] Programas más fáciles
		***[#Pink] Programas más cortos
		***[#Pink] Su depuración es sencilla y rápida
		***[#Pink] El tiempo de desarrollo es aceptable
	**[#Gray] Tipos
		***[#Pink] Funcional
			****[#Orange] Sus funciones son de orden superior
			****[#Orange] La evaluación es perezosa
		***[#Pink] Lógica
			****[#Orange] Es declarativo
			****[#Orange] Su dirección es con argumentos y resultados
	**[#Gray] Evolución
		***[#Pink] Se crean lenguajes de alto nivel
		***[#Pink] Las secuencias son directas
	**[#Gray] Enfoque
		***[#Pink] Se adapta al programador
		***[#Pink] Las secuencias son directas

@endmindmap
```


# Lenguaje de programación funcional (2015)
```plantuml
@startmindmap
*[#Yellow] Programación Funcional
	**[#Pink] ¿Qué es?
		***[#Gray] Es un paradigma de programación declarativo basado en funciones matemáticas
	**[#Pink] Características
		***[#Gray] Funciones matemáticas puras
			****[#Orange] Recibir funciones con parametros de entrada
			****[#Orange] Aceptar parametros de otras funciones
		***[#Gray] Memorización
			****[#Orange] Almacenar valores de la expresión evaluada
			****[#Orange] Es obligatorio
	**[#Pink] Ventajas
		***[#Gray] Su transparencia
		***[#Gray] Parámetros de entrada
		***[#Gray] Ejecución con mayor facilidad
	**[#Pink] Evaluaciones
		***[#Gray] Perezosa
			****[#Orange] Retrasa el cálculo de una expresión para evitar que se repita
		***[#Gray] No estricta
			****[#Orange] No necesita valores para desarrollarse
		***[#Gray] Corto Circuito
			****[#Orange] Se aplica en algunos lenguajes de programación

	**[#Pink] Enfoque
		***[#Gray] Se acoplan a la forma de pensar del programador
		***[#Gray] No se especifíca el control de memoria
		***[#Gray] Desechar asignaciones

	**[#Gray] Su paradigma se rige por
		***[#Pink] Modelo Von Nuemann
			****[#Orange] Los programas se deben almacenar en la máquina antes de ser ejecutada
		***[#Pink] Modelo de computación
			****[#Orange] Conjunto de operaciones permitidas en el cómputo en cuanto a tiempo de ejecución o espacio de memoria

@endmindmap
```
